package com.epam;

import java.util.LinkedHashMap;
import java.util.Map;

public class GameMenu {

    private Map<String, String> startMenu;
    private Map<String, String> continueMenu;
    private Map<String, String> fightMenu;
    private Map<String, String> robotMenu;

    public GameMenu() {
        startMenu = new LinkedHashMap<>();
        continueMenu = new LinkedHashMap<>();
        fightMenu = new LinkedHashMap<>();
        robotMenu = new LinkedHashMap<>();

        startMenu.put("0", "0 - Exit.");
        startMenu.put("1", "1 - Start fight.");
        startMenu.put("2", "2 - Change robot's name.");


        continueMenu.put("0", "0 - Exit game...");
        continueMenu.put("1", "1 - Continue game...");
        continueMenu.put("2", "2 - Start new fight.");
        continueMenu.put("3", "3 - Change robot.");
        continueMenu.put("4", "4 - Get more information about robots.");

        fightMenu.put("1", "1 - Attach enemy.");
        fightMenu.put("2", "2 - Repair health.");
        fightMenu.put("3", "3 - Back.");

        robotMenu.put("0", "0 - Exit.");
        robotMenu.put("1", "1 - Military.");
        robotMenu.put("2", "2 - Humanoid.");
        robotMenu.put("3", "3 - Transformer.");
        robotMenu.put("4", "4 - Choose randomly.");
        robotMenu.put("5", "5 - Get more information about robots.");

    }

    public void printStartMenu() {
        for (String i : startMenu.values()) {
            System.out.println(i);
        }
    }

    public void printContinuetMenu() {
        for (String i : continueMenu.values()) {
            System.out.println(i);
        }
    }

    public void printFightMenu() {
        for (String i : fightMenu.values()) {
            System.out.println(i);
        }
    }

    public void printRobotMenu() {
        for (String i : robotMenu.values()) {
            System.out.println(i);
        }
    }
}
