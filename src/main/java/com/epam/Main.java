package com.epam;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        GameMenu menu = new GameMenu();
        Game game = new Game();

        final String computerDefaultName = "ComputerDroid";
        final String myDefaultName = "MyDroid";

        Scanner scanner = new Scanner(System.in);

        Droid droid1 = game.getMyRobot(myDefaultName);
        Droid droid2 = game.getRandomRobot(computerDefaultName);

        String choose;

        game.showDroids(droid1, droid2);

        label:
        while (true) {
            menu.printStartMenu();
            choose = scanner.nextLine();
            switch (choose) {
                case "1":
                    if (game.fight(droid1, droid2)) {
                        //якщо можна продовжити гру
                        //тобто хп ще є але ви вийшли з файту

                        //якщо змінили робота то не можна продовжити
                        // гру і тоді спрацює цей іф і викличеться
                        // страт меню
                        if (game.continueGame(droid1, droid2)) {
                            droid2 = game.getRandomRobot(droid2.getName());
                            droid1 = game.getMyRobot(droid1.getName());
                            break;
                        }
                        break label;
                    }
                    break;
                case "2":
                    game.changeName(droid1);
                    game.showDroids(droid1, droid2);
                    break;
                case "0":
                    break label;
                default:
                    System.out.println("Error\nPlease try again!");
                    break;
            }
        }
    }
}
