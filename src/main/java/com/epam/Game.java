package com.epam;

import com.epam.Droids.Humanoid;
import com.epam.Droids.Military;
import com.epam.Droids.Transformer;

import java.util.Formatter;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private GameMenu gameMenu = new GameMenu();

    public boolean isSomeoneDead(Droid droid1, Droid droid2) {
        if (droid1.isDead() || droid2.isDead()) {
            if (droid1.isDead() && droid2.isDead()) {
                System.out.println("\nA draw");//якщо нічия
                printWinnerBoth();
            } else if (droid1.isDead()) {
                System.out.println("\nGame Over, this round won: " + droid2.getName());
                droid2.levelUp();
                System.out.println("\nLevel of " + droid2.getName() + ": " + droid2.getLevel());
                printWinnerComputer();
            } else if (droid2.isDead()) {
                System.out.println("\nGame Over, this round won: " + droid1.getName());
                droid1.levelUp();
                System.out.println("\nLevel of " + droid1.getName() + ": " + droid1.getLevel());
                if (droid1.getLevel() - droid2.getLevel() > 2) {
                    droid2.levelUp();
                    droid2.levelUp();
                }
                printWinnerYou();
            }
            System.out.println("\n");
            setMaxHp(droid1, droid2);
            return true;
        }
        return false;
    }

    public void attack(Droid droid1, Droid droid2) {
        final int attackRobot1 = droid1.attack();
        final int attackRobot2 = droid2.attack();

        droid2.loseHealth(attackRobot1);//droid 2 приймає атаку від дроїда 1
        droid1.loseHealth(attackRobot2);//droid 1 приймає атаку від дроїда 2

        System.out.print((char) 27 + "[31m" + droid1.getKind() + ", " + droid1.getName() + " ");
        droid1.showHealthAfterAttack(attackRobot2);

        System.out.print(droid2.getKind() + ", " + droid2.getName() + " ");
        droid2.showHealthAfterAttack(attackRobot1);
    }

    public void repairingRobots(Droid droid1, Droid droid2) {
        final int repairDroid1 = droid1.repair();
        final int repairDroid2 = droid2.repair();

        System.out.print((char) 27 + "[31m" + droid1.getKind() + ", " + droid1.getName() + " ");
        droid1.showHealthRepair(repairDroid1);

        System.out.print(droid2.getName() + " ");
        droid2.showHealthRepair(repairDroid2);
    }

    /**
     * якщо fight повертаж true то значить можна викликати continue menu
     */
    public boolean fight(Droid droid1, Droid droid2) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        boolean canContinue = false;

        String choose;
        attack(droid1, droid2);
        while (flag) {
            if (isSomeoneDead(droid1, droid2)) {
                break;
            }

            printUser(droid1, droid2);
            gameMenu.printFightMenu();

            choose = scanner.nextLine();
            switch (choose) {
                case "1":
                    attack(droid1, droid2);
                    break;
                case "2":
                    repairingRobots(droid1, droid2);
                    break;
                case "3":
                    flag = false;
                    canContinue = true;
                    break;
                default:
                    System.out.println("Error, please try again!");
                    break;
            }
        }
        return canContinue;
    }

    public void setMaxHp(Droid droid1, Droid droid2) {
        droid1.setMaxHp();
        droid2.setMaxHp();
    }

    public Droid getRandomRobot(final String enemyName) {
        //рандомного від 0 до 3 тобто [0;2]
        Droid droid;
        final int max = 3;


        Random rnd = new Random();
        int choose = rnd.nextInt(max);

        if (choose == 0) {
            droid = new Military(enemyName);
            return droid;
        } else if (choose == 1) {
            droid = new Humanoid(enemyName);
            return droid;
        } else if (choose == 2) {
            droid = new Transformer(enemyName);
            return droid;
        }
        return null;

    }

    public static void showRobots(){
        new Military().showInfo();
        new Humanoid().showInfo();
        new Transformer().showInfo();
    }

    public Droid getMyRobot(final String myName) {

        Droid droid1;

        Scanner scanner = new Scanner(System.in);
        String choose;


        label:
        while (true) {
            gameMenu.printRobotMenu();
            choose = scanner.nextLine();
            switch (choose) {
                case "1":
                    droid1 = new Military(myName);
                    break label;
                case "2":
                    droid1 = new Humanoid(myName);
                    break label;
                case "3":
                    droid1 = new Transformer(myName);
                    break label;
                case "4":
                    droid1 = getRandomRobot(myName);
                    break label;
                case "5":
                    showRobots();
                    break;
                case "0":
                    System.exit(0);
                default:
                    System.out.println("Error\nPlease try again!");
                    break;
            }
        }
        return droid1;
    }

    public boolean continueGame(Droid droid1, Droid droid2) {
        Scanner scanner = new Scanner(System.in);
        String choose;
        boolean isChangedRobots = false;


        label:
        while (true) {
            gameMenu.printContinuetMenu();
            choose = scanner.nextLine();
            switch (choose) {
                case "1":
                    if (fight(droid1, droid2)) {
                        //якщо захочемо другий раз нажати continue
                        //але хп ще є
                        if (continueGame(droid1, droid2)) {
                            droid2 = getRandomRobot(droid2.getName());
                            droid1 = getMyRobot(droid1.getName());
                            break;
                        }
                    }
                    break label;
                case "2":
                    setMaxHp(droid1, droid2);
                    if (fight(droid1, droid2)) {
                        if (continueGame(droid1, droid2)) {
                            droid2 = getRandomRobot(droid2.getName());
                            droid1 = getMyRobot(droid1.getName());
                            break;
                        }
                    }
                    break label;
                case "3":
                    isChangedRobots = true;
                    break label;
                case "4":
                    droid1.showInfo();
                    droid2.showInfo();
                    break;
                case "0":
                    break label;
                default:
                    System.out.println("Error\nPlease try again!");
                    break;
            }
        }

        return isChangedRobots;
    }

    public void showDroids(Droid droid1, Droid droid2) {
        System.out.println("\n");
        System.out.println(droid1);
        droid1.showInfo();
        System.out.println("\n");
        System.out.println(droid2);
        droid2.showInfo();
        System.out.println("\n");
    }

    public void changeName(Droid droid) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nEnter a new name of your robot:");
        String name = scanner.nextLine();
        droid.reName(name);
    }

    private void printUser(Droid you, Droid computer) {
        Formatter fmt = new Formatter();
        fmt.format("%11s", "");
        fmt.format((char) 27 + "[32m" + you.getName() + " health: " + you.getHealth()
                + "%107s", computer.getName() + " health: " + computer.getHealth() + "\n");
        fmt.format("%19s", (char) 27);
        fmt.format("[30m____" + "%105s", "^___^\n");
        fmt.format("%24s", "( x ^ x )");
        fmt.format("%105s", "( o ^ o )\n");
        fmt.format("%23s", "/|---|\\");
        System.out.print(fmt);
        printUser2();
    }

    private void printUser2() {
        printWeapon1();
        Formatter f = new Formatter();
        f.format("%46s", (char) 27 + "[33mQ" + (char) 27 + "[30m--^|---|\\\n");
        f.format((char) 27 + "[30m" + "%24s", "/ |---| \\");
        f.format("%105s", "|---| \\\n");
        f.format("%22s", "|- -|");
        f.format("%105s", "|- -|\n");
        f.format("%22s", "B' 'B");
        f.format("%104s", "B' 'B");
        System.out.println(f);
        printUserPart2();
    }

    private void printUserPart2() {
        System.out.println(" ");
        Formatter formatter = new Formatter();
        formatter.format("%19s", (char) 27);
        formatter.format("[30m____" + "%105s", "^___^\n");
        formatter.format("%24s", "( o ^ o )");
        formatter.format("%105s", "( x ^ x )\n");
        formatter.format("%41s", (char) 27 + "[30m/|---|^--" + (char) 27 + "[31mQ" + (char) 27 + "[30m");
        System.out.print(formatter);
        printWeapon2();
        printUserPart22();
    }

    private void printUserPart22() {
        Formatter f = new Formatter();
        f.format((char) 27 + "[30m" + "%35s", "/|---|\\\n");
        f.format((char) 27 + "[30m" + "%22s", "/ |---|");
        f.format("%107s", "/ |---| \\\n");
        f.format("%22s", "|- -|");
        f.format("%105s", "|- -|\n");
        f.format("%22s", "B' 'B");
        f.format("%104s", "B' 'B");
        System.out.println(f);
    }

    private void printWeapon2() {
        Formatter form = new Formatter();
        Formatter f = new Formatter();

        f.format("%20s", (char) 27 + "[31mQ");
        System.out.print(f);
        for (int i = 3; i < 24; i += 6) {
            form.format("%" + i + "s", (char) 27 + "[31mQ");
            System.out.print(form);
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException. In printWeapon2(). " + e.getMessage());
            }

        }
    }

    private void printWeapon1() {
        for (int j = 24; j > 6; j -= 3) {
            String str = String.format("%" + j + "s", (char) 27 + "[33mQ");

            System.out.print(str);
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException. In printWeapon1(). " + e.getMessage());
            }
        }
    }

    private static void printWinnerYou() {
        Formatter formatter = new Formatter();
        formatter.format("%26s", (char) 27 + "[30m____\n");
        formatter.format("%24s", "\\( o ^ o )/\n");
        formatter.format("%27s", (char) 27 + "[30m\\|---|/\n");
        formatter.format("%21s", "|- -|\n");
        formatter.format("%21s", "|- -|\n");
        formatter.format("%20s", "B' 'B");
        System.out.println(formatter);
    }

    private static void printWinnerComputer() {
        Formatter formatter = new Formatter();
        formatter.format("%26s", (char) 27 + "[30m^___^\n");
        formatter.format("%24s", "\\( o ^ o )/\n");
        formatter.format("%27s", (char) 27 + "[30m\\|---|/\n");
        formatter.format("%21s", "|- -|\n");
        formatter.format("%21s", "|- -|\n");
        formatter.format("%20s", "B' 'B");
        System.out.println(formatter);
    }

    private static void printWinnerBoth() {
        Formatter formatter = new Formatter();
        formatter.format("%26s", (char) 27 + "[30m____");
        formatter.format("%26s", (char) 27 + "[30m^___^\n");
        formatter.format("%24s", "\\( o ^ o )/");
        formatter.format("%21s", "\\( o ^ o )/\n");
        formatter.format("%27s", (char) 27 + "[30m\\|---|/");
        formatter.format("%26s", (char) 27 + "[30m\\|---|/\n");
        formatter.format("%21s", "|- -|");
        formatter.format("%21s", "|- -|\n");
        formatter.format("%21s", "|- -|");
        formatter.format("%21s", "|- -|\n");
        formatter.format("%21s", "B' 'B");
        formatter.format("%20s", "B' 'B");
        System.out.println(formatter);
    }

}
