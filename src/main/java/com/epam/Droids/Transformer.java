package com.epam.Droids;

import com.epam.Droid;

public class
Transformer extends Droid {

    private int maxHealth = 110;
    private int repairing = 8;//множник відновлення
    private int damage = 9;//множник атаки

    private static final int PER_LEVELUP_HEALTH = 15;
    private static final int PER_LEVELUP_REPAIRING = 3;
    private static final int PER_LEVELUP_DAMAGE = 3;

    public Transformer() {
        super("NoName");

        super.setHealth(maxHealth);
        super.setRepairing(repairing);
        super.setDamage(damage);

    }

    public Transformer(String name) {
        super(name);
        super.setHealth(maxHealth);
        super.setRepairing(repairing);
        super.setDamage(damage);
    }

    public void levelUp() {
        super.levelUp();

        maxHealth += PER_LEVELUP_HEALTH;
        damage += PER_LEVELUP_DAMAGE;
        repairing += PER_LEVELUP_REPAIRING;
    }

    @Override
    public void setMaxHp() {
        super.setHealth(maxHealth);
    }

    @Override
    public int repair() {
        int repair = getRandomInt(min * repairing, max * repairing);

        if ((this.getHealth() + repair >= maxHealth)) {//щоб не вийшло за межі
            this.setHealth(maxHealth);
            repair = 0;
        } else {
            this.setMoreHealth(repair);
        }
        return repair;
    }

    @Override
    public int attack() {
        return getRandomInt(min * damage, max * damage);
    }


    @Override
    public String getKind() {
        return "Kind: Transformer";
    }

    @Override
    public void showInfo() {
        System.out.println("Kind: Transformer");
        System.out.println("Health: " + maxHealth);
        System.out.println("Repair: " + repairing * min + "-" + repairing * max);
        System.out.println("Damage: " + damage * min + "-" + damage * max);
        System.out.println();
    }
}
