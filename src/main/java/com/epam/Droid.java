package com.epam;

import java.util.Random;

public abstract class Droid {

    private String name;
    private int health;
    private int level;
    private int damage;
    private int repairing;


    /**
     * в дочірніх класах це змінна для найменшого
     * рандомного числа.
     */
    protected int min = 1;
    /**
     * в дочірніх класах це зміння для найбільшого
     * рандомного числа.
     */
    protected int max = 3;


    public Droid() {
        level = 1;
    }


    public Droid(String name) {
        this.name = name;
        level = 1;
    }


    public abstract String getKind();

    public abstract int repair();

    public abstract int attack();

    public abstract void setMaxHp();

    public abstract void showInfo();

    protected void setDamage(int damage) {

        this.damage = damage;
    }


    protected void setRepairing(int repairing) {

        this.repairing = repairing;
    }

    public boolean isDead() {

        return health <= 0;
    }

    protected int getHealth() {

        return health;
    }

    protected void setMoreHealth(int health) {

        this.health += health;
    }

    public int getLevel() {

        return level;
    }

    public String getName() {

        return name;
    }

    protected void setHealth(int health) {

        this.health = health;
    }

    public void levelUp() {

        this.level = level + 1;
    }

    public void reName(String name) {
        this.name = name;
    }

    public void loseHealth(int count) {

        this.health -= count;
    }

    public void showHealthRepair(int repairing) {

        System.out.println((char) 27 + "[31mHealth +" + repairing);
    }

    public void showHealthAfterAttack(int attack) {

        System.out.println((char) 27 + "[31mHealth -" + attack);
    }

    protected static int getRandomInt(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    @Override
    public String toString() {
        return "Droid name: " + getName() +
                "\nhealth: " + getHealth() +
                "\nlevel: " + getLevel();
    }
}
